<?php 


function get_photos($token) {
  $data = json_decode(file_get_contents('https://graph.facebook.com/me/photos?access_token='.$token));
  $ret = array();
  foreach ($data->{data} as $x) {
    if (! $x->{name}) { $x->{name}=""; }
    //compile tags
    $tags = "";
    if ($x->{tags}) {
      $tags = array();
      foreach ($x->{tags}->{data} as $y) {
        $data2 = json_decode(file_get_contents('https://graph.facebook.com/'.$y->{id}.'/picture?redirect=false&type=large&access_token='.$token));
        //echo "\n****".$data2->{data}->{url}."****\n";
        //echo ".";$data2->{data}->{url}
        array_push($tags,array('person_name'=>$y->{name},'person_thumb_url'=>$data2->{data}->{url}));
      }
    }
    array_push($ret,array('id'=>$x->{id},
                          'date'=>$x->{created_time},
                          'description'=>$x->{name},
                          'image_hq_url'=>$x->{images}[0]->{source},
                          'image_sq_url'=>$x->{images}[1]->{source},
                          'image_lq_url'=>$x->{images}[2]->{source},
                          'image_thumb_url'=>$x->{picture},
                          'geo_lat'=>$x->{place}->{location}->{latitude},
                          'geo_lon'=>$x->{place}->{location}->{longitude},
                          'tags'=>$tags
                         ));
  }
  return json_encode($ret);
}

function get_videos($token) {
  $data = json_decode(file_get_contents('https://graph.facebook.com/me/videos?access_token='.$token));
  $ret = array();
  foreach ($data->{data} as $x) {
    if (! $x->{description}) { $x->{description}=""; }
    if (! $x->{name}) { $x->{name}=""; }

    array_push($ret,array('id'=>$x->{id},
                          'date'=>$x->{created_time},
                          'description'=>$x->{description},
                          'video_hq_url'=>$x->{source},
                          'video_thumb_url'=>$x->{picture},
                          'video_poster_url'=>$x->format[count($x->format) - 1]->{picture},
                          'title'=>$x->{name},
                          'video_width'=>$x->format[count($x->format) - 1]->{width},
                          'video_height'=>$x->format[count($x->format) - 1]->{height}           
                         ));
  }
  return json_encode($ret);
}

?>