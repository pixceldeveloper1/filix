
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  </head>

  <body>



    <div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
          <li class=""><fb:login-button scope="public_profile,email,user_photos,user_videos" style="" onlogin="checkLoginState();"></fb:login-button>
          </li>
          <li><div id="status"></div>
          </li>

        </ul>
        <h3 class="text-muted">Demo App</h3>
      </div>

      <?php if (strlen($_GET["p"])>0 ): ?>
      <?php require 'lib_filix.php'; ?>
      <div class="row marketing">
        <div class="col-lg-12"> 
          <div style="width: 650px; word-wrap: break-word; overflow-wrap: break-word;">
          token is '<?php echo $_GET["p"] ?>'</div>
          <br>
          <p>get_photos</p>
          <?php 
          $p = get_photos($_GET["p"]);
          foreach (json_decode($p) as $x) {
            echo "id ".$x->{id}.'<br>';
            echo "date ".$x->{date}.'<br>';
            echo "description ".$x->{description}.'<br>';
            echo "image_hq_url ".$x->{image_hq_url}.'<br>';
            echo 'image_sq <img src="'.$x->{image_sq_url}.'" style="max-width: 550px"><br>';
            echo "image_lq_url ".$x->{image_lq_url}.'<br>';
            echo 'thumbnail '.$x->{image_thumb_url}.'<br>';
            echo "lat ".$x->{geo_lat}.'<br>';
            echo "lon ".$x->{geo_lon}.'<br>';
            echo "tags ".json_encode($x->{tags}).'<br>';
            echo "<hr>";
          }          
          ?>          


          <br>
          <p>get_videos</p>
          <?php 
          $v = get_videos($_GET["p"]);
          foreach (json_decode($v) as $x) {
            echo "id ".$x->{id}.'<br>';
            echo "date ".$x->{date}.'<br>';
            echo "description ".$x->{description}.'<br>';
            echo "video ".'<iframe src="https://www.facebook.com/video/embed?video_id='.$x->{id}.'" width="640" height="480" frameborder="0"></iframe>'.'<br>';
            echo 'thumbnail <img src="'.$x->{video_thumb_url}.'"><br>';
            echo 'poster '.$x->{video_poster_url}.'<br>';
            echo "title ".$x->{title}.'<br>';
            echo "width ".$x->{video_width}.'<br>';
            echo "height ".$x->{video_height}.'<br>';
            echo "<hr>";
          }          
          ?>


          <br>

        </div>


      </div>

      <hr>

      <?php endif; ?>
    



      <div class="footer">
        <p>&copy; 2014</p>
      </div>

    </div> <!-- /container -->




<style>
/* Space out content a bit */
body {
  padding-top: 20px;
  padding-bottom: 20px;
}

/* Everything but the jumbotron gets side spacing for mobile first views */
.header,
.marketing,
.footer {
  padding-right: 15px;
  padding-left: 15px;
}

/* Custom page header */
.header {
  border-bottom: 1px solid #e5e5e5;
}
/* Make the masthead heading the same height as the navigation */
.header h3 {
  padding-bottom: 19px;
  margin-top: 0;
  margin-bottom: 0;
  line-height: 40px;
}

.header li {
  line-height: 40px;
}

/* Custom page footer */
.footer {
  padding-top: 19px;
  color: #777;
  border-top: 1px solid #e5e5e5;
}

/* Customize container */
@media (min-width: 768px) {
  .container {
    max-width: 730px;
  }
}
.container-narrow > hr {
  margin: 30px 0;
}


/* Supporting marketing content */
.marketing {
  margin: 40px 0;
}
.marketing p + h4 {
  margin-top: 28px;
}

/* Responsive: Portrait tablets and up */
@media screen and (min-width: 768px) {
  /* Remove the padding we set earlier */
  .header,
  .marketing,
  .footer {
    padding-right: 0;
    padding-left: 0;
  }
  /* Space out the masthead */
  .header {
    margin-bottom: 30px;
  }
}

.fb_iframe_widget span {
  vertical-align: middle !important;
}

.header li {
  margin-right: 6px;
}

</style>

<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '738177846244362',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.0' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        response.name + ' logged in.';
    });
    <?php
    if (! strlen($_GET["p"])>0) {
      echo 'window.location.href = window.location.href+"?p="+FB.getAccessToken();';
    }
    ?>
  }
</script>

  </body>
</html>
