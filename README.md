## filix - (f)acebook (i)mporter solution

## Requirement

"... a function to import and synchronize users photos and videos along with metadata like tags and faces from Facebook's API to ArchiveCloud's API and data schema..."

We want to allow users to import their Facebook photos and videos with all tags and metadata into their ArchiveCloud account.

For a working example, sign up for a thislife.com account, login, and import from Facebook.

Requirements:
1. Connect to logged in user's Facebook account
2. Authorize to access all photos, videos, and metadata
3. Download either:
3a. specific photos from a specific album, 
3b. an entire album, 
3c. or all media from entire account to ArchiveCloud servers.
4. Download all associated metadata (people, tags with 3.
5. Match Facebook schema to ArchiveCloud schema (attached)
6. When importing, do not import duplicates

Deliverables:
Production ready code for Laravel app   

attachment: [ArchiveCloud schema definition.xlsx](src/a744a70743218483a69192c40ed43a21fdb6a479/ArchiveCloud%20schema%20definition.xlsx?at=master)

## Solution

* __lib_filix.php__ is a library you can use in your site.
* __demo_token.php__ - shows how tokens are generated in  a typical web app
* __demo_app.php__ - Is a demo app that shows the library in action

You can use the library like this:
```
<?php
require('lib_filix.php');
?>
```
The following methods are available:

#### get_photos(token)

Parameters

* token - a valid token, as described in https://developers.facebook.com/docs/facebook-login/access-tokens

Returns users photos in JSON format.

* id - numeric id of the photo
* date - the date of the photo
* descripton - this is the description of the photo
* image_hq_url - highest quality image available
* image_sq_url - 2nd highest quality image available
* image_lq_url - 3rd highest quality image available
* image_thumb_url - thumbnail represenation of image
* geo_lat - latitude
* geo_lon - longitude
* tags - this is an array of person objects.


Example

![json1.png](https://bytebucket.org/pixceldeveloper1/filix/raw/008ae6760568eb57372b922f766653c701ff514f/json1.png?token=074823791286198275ed1faafb290f8bfcfccdc8)


#### get_videos(token)

Parameters

* token - a valid token, as described in https://developers.facebook.com/docs/facebook-login/access-tokens

Returns users videos in JSON format.

* id - numeric id of the photo
* date - the date of the video
* descripton - this is the description of the video
* video_hq_url - highest quality video source available
* video_thumb_url - thumbnail representation of video
* video_poster_url - poster representation of video
* title - title of video
* video_width - horizontal resolution of video
* video_height - vertical resolution of video

Example


![json2.png](https://bytebucket.org/pixceldeveloper1/filix/raw/42cf1e4f05b7bc01bae4eff4a6c7e6c59693dccc/json2.png?token=6080eb844a52985d9ed6a3949c645c5a65a7f76f)


## How to install and run the demo_app.php

See video http://screencast.com/t/y4yEIazzO

## How to remove restrictions imposed by Facebook

At the moment, I use Facebook App ID "738177846244362", which is registered
to sam707707@hotmail.com

Untill we receive approval from Facebook, we can only use
sam707707@hotmail.com Password: kobelakers
as a test user.

Please follow these steps to get approval from Facebook:

* Log in to http://developers.facebook.com/ with sam707707@homail.com
* Navigate to https://developers.facebook.com/apps/738177846244362/dashboard/
* In the (left) "Settings" tab, fill in "App Domains" and "Site URL"
* In the (left) "Status and Review" tab, click "Start a Submission" and choose user_videos and  user_photos and submit.

Alternatively you can use your own facebook account for this task. Just remember to set
the relavant "appId" inside demo_app.php



## Notes

* If any non-fb username is required use pixceldeveloper1@gmail.com Password: kobelakers


## This is screenshot of a test run of demo_app.php

![output.png](https://bytebucket.org/pixceldeveloper1/filix/raw/2788871f71749f75882443afbf1b6ee4a20d19ff/output.png?token=20556d75d83e8c108ce4956b0e0281032912b858)
